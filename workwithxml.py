from os import path
from xml.etree.ElementTree import XML
import zipfile
path=path.join(path.dirname(__file__),'test.docx')
print(path)



def numbering(num,lvl,NUM):#определение метода нумерации списка и начального элемента, с которого начинается нумерация
    word_namespace='{http://schemas.openxmlformats.org/wordprocessingml/2006/main}'
    wnum=numId=word_namespace+'num'
    wabstractNumId=word_namespace+'abstractNumId'
    wabstractNum=word_namespace+'abstractNum'
    value=word_namespace+'val'
    numId=word_namespace+'numId'
    wlvl=word_namespace+'lvl'
    ilvl=word_namespace+'ilvl'
    wstart=word_namespace+'start'
    wnumFmt=word_namespace+'numFmt'
    wlvlText=word_namespace+'lvlText'
    for num1 in NUM.getiterator(wnum):
        if num1.get(numId)==num:
            abstractNumId=num1.find(wabstractNumId).get(value)
            break
    for abstract in NUM.getiterator(wabstractNum):
        if abstract.get(wabstractNumId)==abstractNumId:
            for tagwlvl in abstract.getiterator(wlvl):
                if tagwlvl.get(ilvl)==lvl:
                    numer=(tagwlvl.find(wnumFmt).get(value),tagwlvl.find(wstart).get(value),tagwlvl.find(wlvlText).get(value))
                    break
            break
    return numer

def writelist(path):#непосредственный поиск списка в xml файле полученном путем разархивирования docx
    document = zipfile.ZipFile(path)
    xml_content = document.read('word/document.xml')
    xml_number = document.read('word/numbering.xml')
    document.close()
    tree = XML(xml_content)
    NUM = XML(xml_number)
    word_namespace='{http://schemas.openxmlformats.org/wordprocessingml/2006/main}'
    paragraph=word_namespace+'p'
    numPr=word_namespace+'numPr'
    ilvl=word_namespace+'ilvl'
    numId=word_namespace+'numId'
    TEXT=word_namespace+'t'
    value=word_namespace+'val'
    prelvl=0
    prenum=0
    lvl=0
    num=0
    i=1
    for para in tree.getiterator(paragraph):
        flagoflist=False
        for tmp in para.getiterator(numPr):
            flagoflist=True
            lvl=tmp.find(ilvl).get(value)
            num=tmp.find(numId).get(value)
            texts=""
            if num!=prenum and lvl =='0':
                print("")
                print("список " + str(i))
                i=i+1
                j=0
                prej=[]
            elif lvl!=prelvl and lvl!='0':
                if int(lvl)>int(prelvl):
                    prej.append(j)
                    j=0
                else:
                    j=prej[int(lvl)]
            nmbr=numbering(num,lvl,NUM)
            for node in para.getiterator(TEXT):
                texts=texts+node.text
            if nmbr[0]=='bullet' or nmbr[0]=='none' :
                print("     "*int(lvl)+nmbr[2]+" "+texts)
            elif nmbr[0]=='decimal':
                print("     "*int(lvl)+str(int(nmbr[1])+j)+". "+texts)
                j=j+1
            elif nmbr[0]=='lowerLetter':
                print("     "*int(lvl)+chr(int(nmbr[1])+j-1+ ord('a'))+". "+texts)
                j=j+1
            elif  nmbr[0]=='upperLetter':
                print("     "*int(lvl)+chr( int(nmbr[1])+j-1+ord('A') )+". "+texts)
                j=j+1
            else: # здесь должны быть проверки для правильного отображения списка нумерующегося при помощи римских цифр, слов и других меток, но в данном случае мы просто выводим элементы без меток
                #хотя функция numbering позволяет нам определить какие метки нужно отображать 
                print(texts)
        if (flagoflist!=True):
            simpletexts=""
            for node1 in para.getiterator(TEXT):
                simpletexts=simpletexts+node1.text
            if (simpletexts!=""):
                print("")
                print("текст")
                print(simpletexts)
        prelvl=lvl
        prenum=num
writelist(path)
